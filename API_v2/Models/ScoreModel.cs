﻿using API_v2.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_v2._1.Models
{
    public class ScoreModel : ViewModelBase
    {

        private double rankInfo;
        public double RankInfo
        {
            get
            {
                return rankInfo;
            }
            set
            {
                rankInfo = value;
                OnPropertyChanged("RankInfo");
            }
        }


        private int matches;
        public int Matches
        {
            get
            {
                return matches;
            }
            set
            {
                matches = value;
                OnPropertyChanged("Matches");
            }
        }

        private int kills;
        public int Kills
        {
            get
            {
                return kills;
            }
            set
            {
                kills = value;
                OnPropertyChanged("Kills");
            }
        }

        private string kd;
        public string KD
        {
            get
            {
                return kd;
            }
            set
            {
                kd = value;
                OnPropertyChanged("KD");
            }
        }

        private string wins;
        public string Wins
        {
            get
            {
                return wins;
            }
            set
            {
                wins = value;
                OnPropertyChanged("Wins");
            }
        }

        private int score;
        public int Score
        {
            get
            {
                return score;
            }
            set
            {
                score = value;
                OnPropertyChanged("Score");
            }
        }


    }
}
