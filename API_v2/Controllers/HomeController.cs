﻿using API_v2._1.Models;
using API_v2.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using static API_v2.Models.ApiModel;

namespace API_v2.Controllers
{
    public class HomeController : ApiController
    {
        // GET: api/Api
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Api/5
        public void Get(string user)
        {
            //var nick = "to_wina_tuska";
            var mod = new ApiModel();

            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Add("TRN-Api-Key", "58558e89-294f-4fb7-91bd-e9cdca63a8c7");
            var response = httpClient.GetStringAsync(new Uri(string.Format("https://api.fortnitetracker.com/v1/profile/pc/{0}", user))).Result;
            mod= JsonConvert.DeserializeObject<ApiModel>(response);

            return Json(mod, "Student", JsonRequestBehavior.AllowGet);
        }

        // POST: api/Api
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Api/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Api/5
        public void Delete(int id)
        {
        }
    }
}
