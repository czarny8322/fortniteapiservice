﻿namespace UserApi.Models
{
    public class ScoreModel
    {
        public double RankInfo { get; set; }
        public int Matches { get; set; }
        public int Kills { get; set; }
        public string KD { get; set; }
        public string Wins { get; set; }
        public int Score { get; set; }
    }
}
