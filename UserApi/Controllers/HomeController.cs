﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using UserApi.Models;
using UserApi.Models;

namespace UserApi.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {            
            return View();
        }

        public ActionResult GetUserStat(string user)
        {
            //http://localhost:62635/Home/GetUserStat?user=ninja

            if (!string.IsNullOrEmpty(user))
            {
                var httpClient = new HttpClient();
                httpClient.DefaultRequestHeaders.Add("TRN-Api-Key", "58558e89-294f-4fb7-91bd-e9cdca63a8c7");
                var response = httpClient.GetStringAsync(new Uri(string.Format("https://api.fortnitetracker.com/v1/profile/pc/{0}", user))).Result;
                ApiModel myApiModel = JsonConvert.DeserializeObject<ApiModel>(response);

                ScoreModel model = new ScoreModel
                {
                    Score = myApiModel.recentMatches[0].score,
                    KD = myApiModel.stats.p2.kd.value,
                    Kills = myApiModel.recentMatches[0].kills,
                    Matches = myApiModel.recentMatches[0].matches,
                    RankInfo = myApiModel.recentMatches[0].trnRating,
                    Wins = myApiModel.stats.p2.winRatio.value,
                };


                return Json(model, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("no data", JsonRequestBehavior.AllowGet);
            }
            
        }
    }
}

